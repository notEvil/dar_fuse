import dar_fuse
import argparse
import datetime
import logging
import os
import os.path as o_path

logging.basicConfig(
    level=os.environ.get("PYTHON_LOG_LEVEL", "WARNING"),
    format="%(asctime)s %(levelname)s %(message)s",
)


parser = argparse.ArgumentParser()

parser.add_argument("source", help="Path to archive or directory containing archives.")
parser.add_argument("target", help="Path to target directory.")
parser.add_argument(
    "--foreground", action="store_true", default=False, help="Run in foreground."
)
parser.add_argument(
    "--time", help="Time of view. Format: %%Y-%%m-%%d[[ %%H:%%M:%%S] %%z]."
)
parser.add_argument(
    "--no-delay",
    dest="no_delay",
    action="store_true",
    default=False,
    help="Don't delay removal of extracted files.",
)

arguments = parser.parse_args()


if o_path.isdir(arguments.source):
    paths = [
        o_path.join(arguments.source, name)
        for name in os.listdir(arguments.source)
        if name.endswith(".dar")
    ]

else:
    paths = [arguments.source]

if arguments.time is not None:
    for format in ["%Y-%m-%d", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S %z"]:
        try:
            time = datetime.datetime.strptime(arguments.time, format)

        except ValueError:
            continue

        break

    else:
        raise ValueError(arguments.time)

else:
    time = None

dar_fuse.main(
    paths,
    arguments.target,
    in_foreground=arguments.foreground,
    time_=time,
    delay_removal=not arguments.no_delay,
)
