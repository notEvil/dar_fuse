import fuse
import libdar
import contextlib
import errno
import inspect
import logging
import os
import os.path as o_path
import re
import shutil
import stat
import tempfile
import time
import threading
import traceback
import weakref


def logging_class(type):
    missing_attributes = set()

    class _LoggingClass(type):
        def __getattribute__(self, name):
            try:
                result = super().__getattribute__(name)

            except AttributeError:
                if name not in missing_attributes:
                    missing_attributes.add(name)
                    logging.debug("missing attribute {}".format(repr(name)))

                raise

            if inspect.ismethod(result):
                result = _logging_method(result, name, self)

            return result

        def __repr__(self):
            if type.__repr__ is object.__repr__:
                result = "<{}.{} object at {}>".format(
                    type.__module__, type.__qualname__, hex(id(self))
                )
                return result

            result = super().__repr__()
            return result

    return _LoggingClass


class _logging_method:
    def __init__(self, method, method_name, object):
        self.method = method
        self.method_name = method_name
        self.object = object

    def __call__(self, *args, **kwargs):
        logging.debug(
            "call {}.{}({}, {})".format(
                repr(self.object),
                self.method_name,
                ", ".join(repr(argument) for argument in args),
                ", ".join(
                    "{}={}".format(name, repr(object))
                    for name, object in kwargs.items()
                ),
            )
        )
        try:
            result = self.method(*args, **kwargs)

        except:
            logging.debug(traceback.format_exc())
            raise

        return result


class _FsObjectType:
    pass


_FILE_TYPE = _FsObjectType()
_DIRECTORY_TYPE = _FsObjectType()
_SYMBOLIC_LINK_TYPE = _FsObjectType()

_FS_OBJECT_TYPES = {
    _FILE_TYPE: stat.S_IFREG,
    _DIRECTORY_TYPE: stat.S_IFDIR,
    _SYMBOLIC_LINK_TYPE: stat.S_IFLNK,
}


class _Permission:
    pass


_USER_READ_PERMISSION = _Permission()
_USER_WRITE_PERMISSION = _Permission()
_USER_EXECUTE_PERMISSION = _Permission()
_GROUP_READ_PERMISSION = _Permission()
_GROUP_WRITE_PERMISSION = _Permission()
_GROUP_EXECUTE_PERMISSION = _Permission()
_OTHER_READ_PERMISSION = _Permission()
_OTHER_WRITE_PERMISSION = _Permission()
_OTHER_EXECUTE_PERMISSION = _Permission()

PERMISSIONS = {
    _USER_READ_PERMISSION: stat.S_IRUSR,
    _USER_WRITE_PERMISSION: stat.S_IWUSR,
    _USER_EXECUTE_PERMISSION: stat.S_IXUSR,
    _GROUP_READ_PERMISSION: stat.S_IRGRP,
    _GROUP_WRITE_PERMISSION: stat.S_IWGRP,
    _GROUP_EXECUTE_PERMISSION: stat.S_IXGRP,
    _OTHER_READ_PERMISSION: stat.S_IROTH,
    _OTHER_WRITE_PERMISSION: stat.S_IWOTH,
    _OTHER_EXECUTE_PERMISSION: stat.S_IXOTH,
}


_FILE_TYPE_CODE = 102
_DIRECTORY_TYPE_CODE = 100
_SYMLINK_TYPE_CODE = 108  # TODO correct?

_TYPE_CODES = {
    _FILE_TYPE_CODE: _FILE_TYPE,
    _DIRECTORY_TYPE_CODE: _DIRECTORY_TYPE,
    _SYMLINK_TYPE_CODE: _SYMBOLIC_LINK_TYPE,
}


@contextlib.contextmanager
def _dar():
    libdar.get_version()

    try:
        yield

    finally:
        libdar.close_and_clean()


class BaseOperations(fuse.Operations):
    def __init__(self):
        super().__init__()

        self._open_files = {}

    def _unknown(self, name, args, kwargs):
        print()
        print(name)
        print("args =", repr(args))
        print("kwargs =", repr(kwargs))
        result = getattr(super(), name)(*args, **kwargs)
        print("=", repr(result))
        return result

    # def access(self, path, amode):
    #     self._get_fs_object_info_(path)

    def flush(self, path, file_handle):
        file = self._open_files[file_handle]
        file.flush()

    def getattr(self, path, file_handle):
        if path == "/":
            result = super().getattr("/", file_handle)
            result.update([("st_uid", os.getuid()), ("st_gid", os.getgid())])
            return result

        fs_object_info = self._get_fs_object_info_(path)

        mode = 0
        mode |= _FS_OBJECT_TYPES[fs_object_info.get_type()]
        for permission in fs_object_info.get_permissions():
            mode |= PERMISSIONS[permission]

        result = dict(
            st_mode=mode,
            st_size=fs_object_info.get_size(),
            st_uid=fs_object_info.get_user_id(),
            st_gid=fs_object_info.get_group_id(),
            st_ctime=fs_object_info.get_change_time(),
            st_mtime=fs_object_info.get_modification_time(),
            st_atime=fs_object_info.get_access_time(),
        )
        return result

    getxattr = None

    def open(self, path, flags):
        file = self._open_file(path, flags)
        result = id(file)
        self._open_files[result] = file
        return result

    def read(self, path, size, offset, file_handle):
        file = self._open_files[file_handle]
        file.seek(offset)
        result = file.read(size)
        return result

    def readdir(self, path, file_handle):
        result = [".", ".."]
        result.extend(self._get_names(path))
        return result

    def readlink(self, path):
        fs_object_info = self._get_fs_object_info_(path)

        result = fs_object_info.get_link_path()
        return result

    def release(self, path, file_handle):
        file = self._open_files.pop(file_handle)
        file.close()

    def _get_fs_object_info_(self, path):
        try:
            result = self._get_fs_object_info(path)

        except FileNotFoundError as error:
            raise fuse.FuseOSError(errno.ENOENT) from error

        return result

    def _get_names(self, path):
        raise NotImplementedError

    def _get_fs_object_info(self, path):
        raise NotImplementedError

    def _open_file(self, path, flags):
        raise NotImplementedError


class FsObjectInfo:
    def get_type(self):
        raise NotImplementedError

    def get_size(self):
        raise NotImplementedError

    def get_user_id(self):
        raise NotImplementedError

    def get_group_id(self):
        raise NotImplementedError

    def get_permissions(self):
        raise NotImplementedError

    def get_change_time(self):
        raise NotImplementedError

    def get_modification_time(self):
        raise NotImplementedError

    def get_access_time(self):
        raise NotImplementedError

    def get_link_path(self):
        raise NotImplementedError


class TestOperations(BaseOperations):
    def _get_names(self, path):
        result = ["file", "directory"]
        return result

    def _get_fs_object_info(self, path):
        import datetime

        result = FsObjectInfo()

        if path.endswith("/file"):
            result.get_type = lambda: _FILE_TYPE
            result.get_permissions = lambda: {_USER_READ_PERMISSION}
            result.get_size = lambda: 64

        else:
            result.get_type = lambda: _DIRECTORY_TYPE
            result.get_permissions = lambda: {
                _USER_READ_PERMISSION,
                _USER_EXECUTE_PERMISSION,
            }
            result.get_size = lambda: 0

        result.get_user_id = os.getuid
        result.get_group_id = os.getgid

        time = datetime.datetime.utcnow().timestamp()
        result.get_change_time = (
            result.get_modification_time
        ) = result.get_access_time = lambda: time

        return result

    def _open_file(self, path, flags):
        import io

        result = io.BytesIO(
            b"1234567891123456789212345678931234567894123456789512345678961234"
        )
        return result


# @logging_class
class DarOperations(BaseOperations):
    DAR_USER_INTERACTION = None

    def __init__(self, paths, time_=None, delay_removal=True):
        super().__init__()

        self.paths = paths
        self.time = time_
        self.delay_removal = delay_removal

        paths = sorted(
            {re.search("^(.*?)(\.\d+\.dar)?$", path).group(1) for path in paths}
        )

        if DarOperations.DAR_USER_INTERACTION is None:
            DarOperations.DAR_USER_INTERACTION = _DarUserInteraction()

        if time_ is not None:
            time_ = time_.timestamp()

        self._names = {}
        self._fs_object_infos = {}

        for path in paths:
            logging.info("loading {}".format(repr(path)))

            base_path, file_name = o_path.split(path)
            archive = libdar.archive(
                DarOperations.DAR_USER_INTERACTION,
                libdar.path(base_path),
                file_name,
                "dar",
                libdar.archive_options_read(),
            )

            for list_entry, base_path in _get_list_entries("", archive):
                if False:
                    import pprint

                    pprint.pprint(
                        {
                            name: getattr(list_entry, name)
                            for name in dir(list_entry)
                            if not name.startswith("__")
                        }
                    )
                    raise Exception

                path = o_path.join(base_path, list_entry.get_name())
                fs_object_info = _DarFsObjectInfo(list_entry, archive)
                change_time = _get_change_time(fs_object_info)

                if time_ is not None and time_ < change_time:
                    continue

                self._names.setdefault(base_path, set()).add(list_entry.get_name())

                current_fs_object_infos = self._fs_object_infos.setdefault(path, [])

                if len(current_fs_object_infos) != 0:
                    first_fs_object_info = current_fs_object_infos[0]
                    if (
                        not first_fs_object_info.get_is_delta()
                        and change_time < _get_change_time(first_fs_object_info)
                    ):
                        continue

                if fs_object_info.get_is_delta():
                    current_fs_object_infos.append(fs_object_info)

                else:
                    current_fs_object_infos[:] = (
                        current_fs_object_info
                        for current_fs_object_info in current_fs_object_infos
                        if change_time < _get_change_time(current_fs_object_info)
                    )

                    if fs_object_info.get_list_entry().is_removed_entry():
                        if len(current_fs_object_infos) == 0:
                            del self._fs_object_infos[path]
                            self._names[base_path].remove(list_entry.get_name())
                        continue

                    current_fs_object_infos.insert(0, fs_object_info)

            logging.info(
                "number of entries: {}".format(repr(len(self._fs_object_infos)))
            )

        logging.info("finished loading")

        for fs_object_infos in self._fs_object_infos.values():
            fs_object_infos.sort(key=_get_change_time)

        self._temporary_paths = {}

        self._temporary_counts = {}
        self._lock = threading.Lock()
        self._locks = weakref.WeakValueDictionary()

        self._release_time = {}
        self._remove_thread = threading.Thread(
            target=self._remove_thread_target, daemon=True
        )

        if delay_removal:
            self._remove_thread.start()

    if False:

        def read(self, path, *args, **kwargs):
            logging.debug("reading from {}".format(repr(path)))
            result = super().read(path, *args, **kwargs)
            return result

    def release(self, path, file_handle):
        super().release(path, file_handle)

        with self._lock:
            lock = self._locks.setdefault(path, threading.Lock())

        with lock:
            count = self._temporary_counts[path]

            if count == 1:
                self._close(path)

            else:
                self._temporary_counts[path] = count - 1

            if self.delay_removal:
                self._release_time[path] = time.monotonic()

    def _get_names(self, path):
        result = self._names.get(path[1:], set())
        return result

    def _get_fs_object_info(self, path):
        fs_object_infos = self._fs_object_infos.get(path[1:], None)
        if fs_object_infos is None:
            raise FileNotFoundError(path)
        result = fs_object_infos[-1]
        return result

    def _open_file(self, path, flags):
        base_path, file_name = o_path.split(path)

        with self._lock:
            lock = self._locks.setdefault(path, threading.Lock())

        with lock:
            count = self._temporary_counts.get(path, 0)

            if count == 0:
                logging.debug("extracting {}".format(repr(path)))

                options = libdar.archive_options_extract()
                options.set_flat(True)
                options.set_what_to_check(libdar.comparison_fields.inode_type)

                target_path = tempfile.mkdtemp()
                self._temporary_paths[path] = target_path

                options.set_subtree(
                    libdar.simple_path_mask(
                        o_path.join(target_path, base_path[1:]), True
                    )
                )
                options.set_selection(libdar.same_path_mask(file_name, True))

                for fs_object_info in self._fs_object_infos[path[1:]]:
                    fs_object_info.get_archive().op_extract(
                        libdar.path(target_path), options
                    )

                if self.delay_removal:
                    count += 1  # prevent remove in release

            else:
                target_path = self._temporary_paths[path]

            self._temporary_counts[path] = count + 1

        result = open(os.open(o_path.join(target_path, file_name), flags), "rb")
        return result

    def _remove_thread_target(self):
        INTERVAL = 1  # assume iteration to be fast
        TTL = 5  # assume files are reopened quickly

        while True:
            time.sleep(INTERVAL)

            now_time = time.monotonic()

            for path, count in list(self._temporary_counts.items()):  # TODO ok?
                if count != 1:
                    continue

                with self._lock:
                    lock = self._locks.setdefault(path, threading.Lock())

                with lock:
                    count = self._temporary_counts[path]

                    if count != 1 or (now_time - self._release_time[path]) < TTL:
                        continue

                    self._close(path)

    def _close(self, path):
        logging.debug("removing {}".format(repr(path)))

        temporary_path = self._temporary_paths.pop(path)
        shutil.rmtree(temporary_path)

        del self._temporary_counts[path]

        if self.delay_removal:
            del self._release_time[path]


def _get_change_time(fs_object_info):
    list_entry = fs_object_info.get_list_entry()
    if list_entry.is_removed_entry():
        result = list_entry.get_removal_date_s()
        return result

    result = max(
        fs_object_info.get_modification_time(), fs_object_info.get_change_time()
    )
    return result


class _DarUserInteraction(libdar.user_interaction):
    def inherited_message(self, message_string):
        logging.debug(message_string)

    def inherited_pause(self, message_string):
        logging.error(message_string)
        result = super().inherited_pause(message_string)
        return result

    if False:

        def inherited_get_string(self, message_string, echo):
            return input(message_string)

        def inherited_get_secu_string(self, message_string, echo):
            return input(message_stringmsg)


def _get_list_entries(path, archive):
    for list_entry in archive.get_children_in_table(path, False):
        type_code = list_entry.get_type()

        if type_code == _FILE_TYPE_CODE:
            if not (
                list_entry.get_data_status()
                not in [libdar.saved_status.fake, libdar.saved_status.not_saved]
                and not list_entry.is_dirty()  # TODO optional?
            ):
                continue

        elif not (
            type_code in [_DIRECTORY_TYPE_CODE, _SYMLINK_TYPE_CODE]
            or list_entry.is_removed_entry()
        ):
            continue

        yield (list_entry, path)

        if type_code == _DIRECTORY_TYPE_CODE:
            yield from _get_list_entries(
                o_path.join(path, list_entry.get_name()), archive
            )


class _DarFsObjectInfo(FsObjectInfo):
    def __init__(self, list_entry, archive):
        super().__init__()

        self.list_entry = list_entry
        self.archive = archive

    def get_list_entry(self):
        result = self.list_entry
        return result

    def get_archive(self):
        result = self.archive
        return result

    def get_type(self):
        result = _TYPE_CODES[self.list_entry.get_type()]
        return result

    def get_size(self):
        type_code = self.list_entry.get_type()

        if type_code in [_FILE_TYPE_CODE, _SYMLINK_TYPE_CODE]:
            result = int(self.list_entry.get_file_size(True))
            return result

        if type_code == _DIRECTORY_TYPE_CODE:
            return 0

        raise NotImplementedError

    def get_user_id(self):
        result = int(self.list_entry.get_uid(False))
        return result

    def get_group_id(self):
        result = int(self.list_entry.get_gid(False))
        return result

    def get_permissions(self):
        permission_string = self.list_entry.get_perm()
        result = {
            permission
            for character, true_character, permission in zip(
                permission_string[-9:],
                "rwx" * 3,
                [
                    _USER_READ_PERMISSION,
                    _USER_WRITE_PERMISSION,
                    _USER_EXECUTE_PERMISSION,
                    _GROUP_READ_PERMISSION,
                    _GROUP_WRITE_PERMISSION,
                    _GROUP_EXECUTE_PERMISSION,
                    _OTHER_READ_PERMISSION,
                    _OTHER_WRITE_PERMISSION,
                    _OTHER_EXECUTE_PERMISSION,
                ],
            )
            if character == true_character
        }
        return result

    def get_change_time(self):
        result = self.list_entry.get_last_change_s()
        return result

    def get_modification_time(self):
        result = self.list_entry.get_last_modif_s()
        return result

    def get_access_time(self):
        result = self.list_entry.get_last_access_s()
        return result

    def get_link_path(self):
        result = self.list_entry.get_link_target()
        return result

    def get_is_delta(self):
        result = self.list_entry.get_data_status() in [
            libdar.saved_status.inode_only,
            libdar.saved_status.delta,
        ]
        return result


def main(
    source_paths, target_path, in_foreground=False, time_=None, delay_removal=True
):
    with _dar():
        fuse.FUSE(
            DarOperations(source_paths, time_=time_, delay_removal=delay_removal),
            target_path,
            foreground=in_foreground,
            ro=True,
        )
