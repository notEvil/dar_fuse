# dar_fuse

This Python package uses the official Python bindings for libdar to access **dar** archives and **fusepy** to provide a read-only filesystem. In short, you can mount a single dar archive, or multiple related dar archives, and use your favourite filesystem browser to find, open or copy files.

There is an AVFS extension with a similar goal, however because its calling dar for each file individually, copy operations of many small files is very slow.

**!!! Beware** that this Python package is not bullet proof (e.g. there are no tests), so don't rely on it when it matters. If it doesn't work as expected, I will try to fix it in reasonable time. If you need more than it provides now, I happily review any pull request :)

## Quickstart

- Install `dar` and `fusepy`
  - e.g. `yay -S dar python-fusepy`
- In `bash` write
`PYTHONPATH=/usr/lib/python3/dist-packages PYTHON_LOG_LEVEL=DEBUG python -m dar_fuse --help`
  - `PYTHONPATH` tells Python where to find the Python bindings for libdar
  - `PYTHON_LOG_LEVEL` is optional
